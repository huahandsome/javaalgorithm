package com.example.hashmapTrie;

import org.junit.Test;

/**
 * Created by huahandsome on 8/9/17.
 */
public class TrieNodeTest {
    @Test
    public void TrieNodeFun(){
        TrieNode root = new TrieNode();

        TrieNode.insert(root, "a");
        TrieNode.insert(root, "b");
        TrieNode.insert(root, "any");
        TrieNode.insert(root, "any");
        TrieNode.insert(root, "anybody");
        TrieNode.insert(root, "baby");
        TrieNode.insert(root, "bad");

        System.out.println(TrieNode.search(root, "a"));
        System.out.println(TrieNode.search(root, "any"));
        System.out.println(TrieNode.search(root, "anybody"));
        System.out.println(TrieNode.search(root, "baby"));
        System.out.println(TrieNode.search(root, "bad"));
        System.out.println("===");
        System.out.println(TrieNode.search(root, "badx"));
        System.out.println(TrieNode.search(root, "an"));
        System.out.println(TrieNode.search(root, "anybo"));
    }
}
