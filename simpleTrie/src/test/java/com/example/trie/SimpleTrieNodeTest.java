package com.example.trie;

import org.junit.Test;

/**
 * Created by huahandsome on 8/9/17.
 */
public class SimpleTrieNodeTest {
    private SimpleTrieNode root = new SimpleTrieNode();

    @Test
    public void testInsertFun(){
        SimpleTrieNode.insert(root, "a");
        SimpleTrieNode.insert(root, "any");
        //add a test case add a duplicated word
        SimpleTrieNode.insert(root, "any");
        SimpleTrieNode.insert(root, "anybody");
        SimpleTrieNode.insert(root, "baby");
        SimpleTrieNode.insert(root, "bad");

        testSearchFun();
    }

    public void testSearchFun(){
        System.out.println(SimpleTrieNode.search(root, "a"));
        System.out.println(SimpleTrieNode.search(root, "any"));
        System.out.println(SimpleTrieNode.search(root, "anybody"));
        System.out.println(SimpleTrieNode.search(root, "baby"));
        System.out.println(SimpleTrieNode.search(root, "bad"));
        System.out.println("------------");
        System.out.println(SimpleTrieNode.search(root, "badx"));
        System.out.println(SimpleTrieNode.search(root, "an"));
        System.out.println(SimpleTrieNode.search(root, "anybo"));
    }
}
