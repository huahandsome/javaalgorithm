package com.example.trie;

import org.junit.Assert;

/**
 * Created by huahandsome on 8/9/17.
 */
public class SimpleTrieNode {

    // Array for Trie
    private boolean end;
    private SimpleTrieNode[] children;

    SimpleTrieNode(){
        end = false;
        children = new SimpleTrieNode[26];
    }

    public static void insert(SimpleTrieNode root, String word){
        Assert.assertTrue((root != null) && (word != null));

        SimpleTrieNode current = root;
        int wordLength = word.length();
        int pos = 0;

        for(int index = 0; index < wordLength; index++){
            pos = word.charAt(index) - 'a';

            if(current.children[pos] == null){
                current.children[pos] = new SimpleTrieNode();
            }

            current = current.children[pos];

            if(index == wordLength - 1){  //the last char
                current.end = true;
            }
        }
    }

    public static boolean search(SimpleTrieNode root, String word){
        Assert.assertTrue((root != null) && (word != null));
        boolean isMatch = false;

        SimpleTrieNode current = root;
        int wordLength = word.length();
        int pos = 0;

        for(int index = 0; index < wordLength; index++){
            pos = word.charAt(index) - 'a';
            current = current.children[pos];

            if(current == null){
                // exclude finding "ab" &"anyz" in "any"
                return false;
            }
        }

        if(current.end){    //exclude "an" in "any"
            isMatch = true;
        }

        return isMatch;
    }
}
