package com.example.hashmapTrie;

import org.junit.Assert;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by huahandsome on 8/9/17.
 */
public class TrieNode {
    private boolean end;
    private HashMap<Character, TrieNode> children;

    TrieNode(){
        end = false;
        children = new HashMap<Character, TrieNode>();
    }

    public static void insert(TrieNode root, String word){
        Assert.assertTrue((root != null) && (word != null) && (word.length() != 0));

        TrieNode currNode = root;

        for(int index = 0; index < word.length(); index++){
            char key = word.charAt(index);

            if(currNode.children.containsKey(key)){
                currNode = currNode.children.get(key);
            }else{
                TrieNode newNode = new TrieNode();
                currNode.children.put(key, newNode);
                currNode = newNode;

                if(index == word.length() - 1){
                    newNode.end = true;
                }
            }
        }
    }

    public static boolean search(TrieNode root, String word){
        Assert.assertTrue((root != null) && (word != null) && (word.length() != 0));
        TrieNode currNode = root;

        for(int index = 0; index < word.length(); index++){
            char key = word.charAt(index);

            currNode = currNode.children.get(key);

            if(currNode == null){
                return false;
            }
        }

        if(currNode.end){
            return true;
        }

        return false;
    }
}
